import { browser, by, element, ElementFinder } from "protractor";

export class AppPage {
  navigateToLogin() {
    return browser.get(browser.baseUrl) as Promise<any>;
  }
  getPhoneInput(): ElementFinder {
    return element(by.id("phone"));
  }
  getPasswordInput(): ElementFinder {
    return element(by.id("password"));
  }
  getButton(): ElementFinder {
    return element(by.css("button"));
  }
  sendLoginInput(phone, password) {
    const phoneInput = this.getPhoneInput();
    const passwordInput = this.getPasswordInput();
    const button = this.getButton();
    phoneInput.sendKeys(phone);
    passwordInput.sendKeys(password);
    button.click();
  }
  getPageGreeting() {
    return element(by.css("h1"));
  }
}

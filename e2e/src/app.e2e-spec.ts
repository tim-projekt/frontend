import { AppPage } from "./app.po";
import { browser, logging } from "protractor";

describe("workspace-project App", () => {
  let page: AppPage;
  const validPassword = "admin";
  const validPhone = "+48000000000";

  beforeEach(() => {
    page = new AppPage();
    page.navigateToLogin();
  });

  it("Should redirect to home when valid login is performed", () => {
    page.sendLoginInput(validPhone, validPassword);
    expect(page.getPageGreeting().getText()).toEqual("Witaj!");
  });

  afterEach(async () => {
    const logs = await browser
      .manage()
      .logs()
      .get(logging.Type.BROWSER);
    expect(logs).not.toContain(
      jasmine.objectContaining({
        level: logging.Level.SEVERE
      } as logging.Entry)
    );
  });
});

import { Component, OnInit } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { first } from "rxjs/operators";
import { Car } from "../_models/car";
import { Parking } from "../_models/parking";
import { AlertService } from "../_services/alert.service";
import { CarService } from "../_services/car-service";
import { ParkingService } from "../_services/parking.service";

@Component({
  selector: "app-car-rent-admin",
  templateUrl: "./car-rent-admin.component.html",
  styleUrls: ["./car-rent-admin.component.css"]
})
export class CarRentAdminComponent implements OnInit {
  lat = 52.229676;
  lng = 21.012229;

  parkings: Parking[] = [];
  cars: Car[] = [];
  succesMessage = "Samochód usunięty pomyślnie";

  constructor(
    private parkingService: ParkingService,
    private carService: CarService,
    private formBuilder: FormBuilder,
    private alertService: AlertService
  ) {}

  ngOnInit() {
    this.getParkings();
    this.getCars();
  }
  getParkings() {
    this.parkingService
      .getParkings()
      .pipe(first())
      .subscribe(parkings => {
        this.parkings = parkings;
        console.log(this.parkings);
      });
  }
  deleteParking(id) {
    this.parkingService
      .deleteParking(id)
      .pipe(first())
      .subscribe(
        data => {
          this.alertService.success("Parking usunięty pomyślnie", true);
          this.getParkings();
        },
        error => {
          this.alertService.error(error);
        }
      );
  }
  getCars() {
    this.carService
      .getCars()
      .pipe(first())
      .subscribe(cars => {
        this.cars = cars;
        console.log(this.cars);
      });
  }
  deleteCar(id) {
    this.carService
      .deleteCar(id)
      .pipe()
      .subscribe(
        data => {
          this.alertService.success(this.succesMessage, true);
          this.getCars();
        },
        error => {
          this.alertService.error(error);
        }
      );
  }
}

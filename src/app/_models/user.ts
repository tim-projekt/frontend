export interface User {
  email: string;
  phone: string;
  password: string;
  name: string;
  surname: string;
  newPassword: string;
}

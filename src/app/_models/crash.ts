import { Car } from "./car";
import { User } from "./user";

export interface Crash {
  id: number;
  user: User;
  car: Car;
  description: string;
  status?: number;
}

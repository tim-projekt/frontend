export interface Parking {
  id?: number;
  freeSpace: number;
  latLngX: number;
  latLngY: number;
  name: string;
  parkingSpaces: number;
  status: string;
}

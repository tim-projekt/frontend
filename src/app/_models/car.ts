export interface Car {
  id?: number;
  latLngX: number;
  latLngY: number;
  model: string;
  purchaseDate: string;
  registrationNumber: string;
  status: string;
}

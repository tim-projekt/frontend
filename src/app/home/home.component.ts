import { Component, OnDestroy, OnInit } from "@angular/core";
import { Subscription } from "rxjs";
import { User } from "../_models/user";
import { AuthService, Credentials } from "../_services/auth.service";
import { UserService } from "../_services/user.service";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit {
  currentUser: User;
  credentials: Credentials;
  currentUserSubscription: Subscription;
  users: User[] = [];

  constructor(
    private authService: AuthService,
    private userService: UserService
  ) {}

  ngOnInit() {
    this.credentials = this.authService.credentials;
  }
}

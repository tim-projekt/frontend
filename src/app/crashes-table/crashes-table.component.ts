import { animate, state, style, transition, trigger } from "@angular/animations";
import { Component, OnInit, ViewChild } from "@angular/core";
import { MatPaginator, MatSort, MatTableDataSource } from "@angular/material";
import { first } from "rxjs/operators";
import { Crash } from "../_models/crash";
import { AlertService } from "../_services/alert.service";
import { CrashService } from "../_services/crash.service";

@Component({
  selector: "app-crashes-table",
  templateUrl: "./crashes-table.component.html",
  styleUrls: ["./crashes-table.component.css"],
  animations: [
    trigger("detailExpand", [
      state(
        "void",
        style({ height: "0px", minHeight: "0", visibility: "hidden" })
      ),
      state("*", style({ height: "*", visibility: "visible" })),
      transition("void <=> *", animate("225ms cubic-bezier(0.4, 0.0, 0.2, 1)"))
    ])
  ]
})
export class CrashesTableComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  displayedColumns: string[] = [
    "id",
    "email",
    "model",
    "registrationNumber",
    "delete"
  ];
  expandedElement: Crash | null;
  dataSource: MatTableDataSource<Crash>;
  crashes: Crash[] = [];
  succesMessage = "Szkoda usunięta pomyślnie";

  constructor(
    private crashService: CrashService,
    private alertService: AlertService
  ) {}

  ngOnInit() {
    this.getCrashes();
  }
  getCrashes() {
    this.crashService
      .getAll()
      .pipe()
      .subscribe(crashes => {
        this.crashes = crashes;
        this.dataSource = new MatTableDataSource<Crash>(this.crashes);
        this.dataSource.filterPredicate = this.filterPredicate;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
  }
  deleteCrash(id) {
    this.crashService
      .delete(id)
      .pipe(first())
      .subscribe(
        data => {
          this.alertService.success(this.succesMessage, true);
          this.getCrashes();
        },
        error => {
          this.alertService.error(error);
        }
      );
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  filterPredicate(data, filter): boolean {
    return data.user.email.toLowerCase().includes(filter);
  }
}

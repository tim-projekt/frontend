import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { By } from "@angular/platform-browser";
import { Router } from "@angular/router";
import { MaterialModule } from "src/app/material/material.module";
import { AuthService } from "src/app/_services/auth.service";
import { SidenavListComponent } from "./sidenav-list.component";

describe("SidenavListComponent", () => {
  let component: SidenavListComponent;
  let fixture: ComponentFixture<SidenavListComponent>;

  let authServiceSpy: jasmine.SpyObj<AuthService>;
  let routerSpy: jasmine.SpyObj<Router>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MaterialModule, FormsModule, ReactiveFormsModule],
      declarations: [SidenavListComponent],
      providers: [
        { provide: AuthService, useValue: authServiceSpy },
        { provide: Router, useValue: routerSpy }
      ]
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(SidenavListComponent);
        component = fixture.componentInstance;
      });
  }));

  it("should create SidenavListComponent", () => {
    expect(component).toBeTruthy();
  });

  it("should trigger onSidenavClose when user click element on sidenav", async(() => {
    spyOn(component, "onSidenavClose");
    const button = fixture.debugElement.queryAll(By.css("a"))[0];
    button.triggerEventHandler("click", null);

    fixture.whenStable().then(() => {
      expect(component.onSidenavClose).toHaveBeenCalled();
    });
  }));
  it("should trigger logout when user click last element on sidenav", async(() => {
    spyOn(component, "logout");
    const buttons = fixture.debugElement.queryAll(By.css("a"));
    const button = buttons[buttons.length - 1];
    button.triggerEventHandler("click", null);

    fixture.whenStable().then(() => {
      expect(component.logout).toHaveBeenCalled();
    });
  }));
});

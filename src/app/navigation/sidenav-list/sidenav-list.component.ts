import { Component, EventEmitter, Output } from "@angular/core";
import { Router } from "@angular/router";
import { AuthService } from "src/app/_services/auth.service";

@Component({
  selector: "app-sidenav-list",
  templateUrl: "./sidenav-list.component.html",
  styleUrls: ["./sidenav-list.component.css"]
})
export class SidenavListComponent {
  @Output() sidenavClose = new EventEmitter();

  constructor(private router: Router, private authService: AuthService) {}

  public onSidenavClose = () => {
    this.sidenavClose.emit();
  };
  logout() {
    this.authService.logout();
    this.router.navigate(["/login"]);
    this.onSidenavClose();
  }
}

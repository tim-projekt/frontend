import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { finalize } from "rxjs/operators";
import { AlertService } from "../_services/alert.service";
import { AuthService } from "../_services/auth.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  invalidLogin: boolean;
  loginForm: FormGroup;
  isSubmitted = false;
  isLoading = false;
  returnUrl: string;
  redirectTo = "/home";
  errorOccured = false;

  constructor(
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private alertService: AlertService
  ) {
    if (this.authService.credentials) {
      this.router.navigate(["/"]);
    }
    this.loginForm = this.formBuilder.group({
      phone: ["", Validators.required],
      password: ["", Validators.required]
    });
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.redirectTo = params.redirect;
      this.returnUrl = this.route.snapshot.queryParams["returnUrl"] || "/";
    });
  }
  get f() {
    return this.loginForm.controls;
  }

  onSubmit() {
    this.isSubmitted = true;
    this.isLoading = true;

    if (this.loginForm.invalid) {
      return;
    }
    this.authService
      .login(this.loginForm.value)
      .pipe(
        finalize(() => {
          this.loginForm.markAsPristine();
          this.isLoading = false;
        })
      )
      .subscribe(
        resp => {
          console.log("ok");
          if (this.redirectTo) {
            this.router.navigate([this.returnUrl]);
          } else {
            this.router.navigate(["/"], { replaceUrl: true });
            this.invalidLogin = true;
          }
        },
        error => {
          console.log("ERROR");
          this.alertService.error(error);
          this.errorOccured = true;
        }
      );
  }
}

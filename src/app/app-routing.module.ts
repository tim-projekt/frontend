import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AddCarComponent } from "./add-car/add-car.component";
import { AddParkingComponent } from "./add-parking/add-parking.component";
import { AuthGuard } from "./auth.guard";
import { CarRentAdminComponent } from "./car-rent-admin/car-rent-admin.component";
import { CrashesComponent } from "./crashes/crashes.component";
import { HomeComponent } from "./home/home.component";
import { LoginComponent } from "./login/login.component";
import { ParkingsComponent } from "./parkings/parkings.component";
import { RegisterComponent } from "./register/register.component";

const routes: Routes = [
  { path: "", component: HomeComponent, canActivate: [AuthGuard] },
  {
    path: "car-rent-admin",
    component: CarRentAdminComponent,
    canActivate: [AuthGuard]
  },
  { path: "parkings", component: ParkingsComponent, canActivate: [AuthGuard] },
  { path: "add-car", component: AddCarComponent, canActivate: [AuthGuard] },
  {
    path: "add-parking",
    component: AddParkingComponent,
    canActivate: [AuthGuard]
  },
  { path: "crashes", component: CrashesComponent, canActivate: [AuthGuard] },
  { path: "login", component: LoginComponent },
  { path: "register", component: RegisterComponent },

  { path: "**", redirectTo: "" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}

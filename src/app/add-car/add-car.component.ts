import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { finalize } from "rxjs/operators";
import { Car } from "../_models/car";
import { Marker } from "../_models/marker";
import { AlertService } from "../_services/alert.service";
import { CarService } from "../_services/car-service";

@Component({
  selector: "app-add-car",
  templateUrl: "./add-car.component.html",
  styleUrls: ["./add-car.component.css"]
})
export class AddCarComponent implements OnInit {
  lat = 52.229676;
  lng = 21.012229;
  newCarMarker: Marker;
  addCarForm: FormGroup;
  isSubmitted = false;
  isLoading = false;
  succesMessage = "Pomyślnie dodano samochód";

  constructor(
    private carService: CarService,
    private formBuilder: FormBuilder,
    private alertService: AlertService
  ) {}

  ngOnInit() {
    this.newCarMarker = {
      lat: this.lat,
      lng: this.lng,
      label: "Nowy Samochód",
      draggable: true
    };
    this.addCarForm = this.formBuilder.group({
      model: ["", Validators.required],
      registrationNumber: ["", Validators.required],
      purchaseDate: ["", Validators.required]
    });
  }
  onSubmit() {
    this.isSubmitted = true;
    if (this.addCarForm.invalid) {
      return;
    }

    this.isLoading = true;
    const carData: Car = this.addCarForm.value;
    carData.latLngX = this.newCarMarker.lat;
    carData.latLngY = this.newCarMarker.lng;
    carData.status = "ACTIVE";

    this.carService
      .addCar(carData)
      .pipe(
        finalize(() => {
          this.isLoading = false;
        })
      )
      .subscribe(
        data => {
          this.alertService.success(this.succesMessage, true);
        },
        error => {
          this.alertService.error(error);
        }
      );
  }
  mapClicked($event) {
    this.newCarMarker.lat = $event.coords.lat;
    this.newCarMarker.lng = $event.coords.lng;
  }

  get f() {
    return this.addCarForm.controls;
  }
}

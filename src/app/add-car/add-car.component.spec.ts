import { AgmCoreModule } from "@agm/core";
import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { FormBuilder, FormsModule, ReactiveFormsModule } from "@angular/forms";
import { environment } from "src/environments/environment";
import { MaterialModule } from "../material/material.module";
import { AlertService } from "../_services/alert.service";
import { CarService } from "../_services/car-service";
import { AddCarComponent } from "./add-car.component";

describe("AddCarComponent", () => {
  let component: AddCarComponent;
  let fixture: ComponentFixture<AddCarComponent>;
  let formBuilder: FormBuilder;
  let alertServiceSpy: jasmine.SpyObj<AlertService>;
  let carServiceSpy: jasmine.SpyObj<CarService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        AgmCoreModule.forRoot({ apiKey: environment.apiKey }),
        MaterialModule,
        FormsModule,
        ReactiveFormsModule
      ],
      declarations: [AddCarComponent],
      providers: [
        AddCarComponent,
        { provide: CarService, useValue: carServiceSpy },
        { provide: AlertService, useValue: alertServiceSpy }
      ]
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(AddCarComponent);
        component = fixture.componentInstance;
        alertServiceSpy = TestBed.get(AlertService);
        carServiceSpy = TestBed.get(CarService);

        formBuilder = new FormBuilder();
        component = new AddCarComponent(
          carServiceSpy,
          formBuilder,
          alertServiceSpy
        );
        component.ngOnInit();
      });
  }));

  it("should create form with car model, registration number and purchase date", () => {
    expect(component.addCarForm.contains("model")).toBeTruthy();
    expect(component.addCarForm.contains("registrationNumber")).toBeTruthy();
    expect(component.addCarForm.contains("purchaseDate")).toBeTruthy();
  });
  it("should set car model, registration number and purchase date controls as required", () => {
    const modelControl = component.addCarForm.get("model");
    const registrationControl = component.addCarForm.get("registrationNumber");
    const dateControl = component.addCarForm.get("purchaseDate");

    modelControl.setValue("");
    registrationControl.setValue("");
    dateControl.setValue("");

    expect(modelControl.valid).toBeFalsy();
    expect(registrationControl.valid).toBeFalsy();
    expect(dateControl.valid).toBeFalsy();
  });
});

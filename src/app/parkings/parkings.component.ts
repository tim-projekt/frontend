import { Component, OnInit } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { first } from "rxjs/operators";
import { Parking } from "../_models/parking";
import { AlertService } from "../_services/alert.service";
import { ParkingService } from "../_services/parking.service";

@Component({
  selector: "app-parkings",
  templateUrl: "./parkings.component.html",
  styleUrls: ["./parkings.component.css"]
})
export class ParkingsComponent implements OnInit {
  lat = 52.229676;
  lng = 21.012229;

  parkings: Parking[] = [];
  constructor(
    private parkingService: ParkingService,
    private formBuilder: FormBuilder,
    private alertService: AlertService
  ) {}

  ngOnInit() {
    this.getParkings();
  }
  getParkings() {
    this.parkingService
      .getParkings()
      .pipe(first())
      .subscribe(parkings => {
        this.parkings = parkings;
        console.log(this.parkings);
      });
  }
  deleteParking(id) {
    this.parkingService
      .deleteParking(id)
      .pipe()
      .subscribe(
        data => {
          this.alertService.success("Parking usunięty pomyślnie", true);
          this.getParkings();
        },
        error => {
          this.alertService.error(error);
        }
      );
  }
}

import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { User } from "./_models/user";
import { AuthService } from "./_services/auth.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  currentUser: User;
  title = "car-rent-frontend";
  constructor(private router: Router, private authService: AuthService) {
  }

  logout() {
    this.authService.logout();
    this.router.navigate(["/login"]);
  }
  get isAuthenticated() {
    return this.authService.isAuthenticated();
  }
}

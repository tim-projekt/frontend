import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { finalize } from "rxjs/operators";
import { Marker } from "../_models/marker";
import { Parking } from "../_models/parking";
import { AlertService } from "../_services/alert.service";
import { ParkingService } from "../_services/parking.service";

@Component({
  selector: "app-add-parking",
  templateUrl: "./add-parking.component.html",
  styleUrls: ["./add-parking.component.css"]
})
export class AddParkingComponent implements OnInit {
  lat = 52.229676;
  lng = 21.012229;
  newParkingMarker: Marker;
  addParkingForm: FormGroup;
  isSubmitted = false;
  isLoading = false;
  succesMessage = "Pomyślnie dodano parking";

  constructor(
    private parkingService: ParkingService,
    private formBuilder: FormBuilder,
    private alertService: AlertService
  ) {}

  ngOnInit() {
    this.newParkingMarker = {
      lat: this.lat,
      lng: this.lng,
      label: "Nowy Parking",
      draggable: true
    };
    this.addParkingForm = this.formBuilder.group({
      name: ["", Validators.required],
      parkingSpaces: ["", Validators.required]
    });
  }
  onSubmit() {
    this.isSubmitted = true;
    if (this.addParkingForm.invalid) {
      return;
    }

    this.isLoading = true;
    const parkingData: Parking = this.addParkingForm.value;
    parkingData.latLngX = this.newParkingMarker.lat;
    parkingData.latLngY = this.newParkingMarker.lng;
    parkingData.freeSpace = parkingData.parkingSpaces;
    parkingData.status = "ACTIVE";

    console.log(parkingData);
    this.parkingService
      .addParking(parkingData)
      .pipe(
        finalize(() => {
          this.isLoading = false;
        })
      )
      .subscribe(
        data => {
          this.alertService.success(this.succesMessage, true);
        },
        error => {
          this.alertService.error(error);
        }
      );
  }
  mapClicked($event) {
    this.newParkingMarker.lat = $event.coords.lat;
    this.newParkingMarker.lng = $event.coords.lng;
  }
  clickedMarker(label: string, index: number) {
    console.log(`clicked the marker: ${label || index}`);
  }

  get f() {
    return this.addParkingForm.controls;
  }
}

import { AgmCoreModule } from "@agm/core";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { environment } from "./../environments/environment";
import { AddCarComponent } from "./add-car/add-car.component";
import { AddParkingComponent } from "./add-parking/add-parking.component";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { CarRentAdminComponent } from "./car-rent-admin/car-rent-admin.component";
import { CdkDetailRowDirective } from "./crashes-table/cdk-detail-row-directive";
import { CrashesTableComponent } from "./crashes-table/crashes-table.component";
import { CrashesComponent } from "./crashes/crashes.component";
import { HomeComponent } from "./home/home.component";
import { LoginComponent } from "./login/login.component";
import { MaterialModule } from "./material/material.module";
import { HeaderComponent } from "./navigation/header/header.component";
import { SidenavListComponent } from "./navigation/sidenav-list/sidenav-list.component";
import { ParkingsComponent } from "./parkings/parkings.component";
import { RegisterComponent } from "./register/register.component";
import { AuthInterceptor } from "./_helpers/auth-interceptor";
import { ErrorInterceptor } from "./_helpers/error-interceptor";
import { AuthService } from "./_services/auth.service";
import { CarService } from "./_services/car-service";
import { CrashService } from "./_services/crash.service";
import { ParkingService } from "./_services/parking.service";
import { UserService } from "./_services/user.service";




@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    RegisterComponent,
    SidenavListComponent,
    HeaderComponent,
    AddCarComponent,
    AddParkingComponent,
    ParkingsComponent,
    CarRentAdminComponent,
    CrashesComponent,
    CrashesTableComponent,
    CdkDetailRowDirective
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    AgmCoreModule.forRoot({
      apiKey: environment.apiKey
    })
  ],
  providers: [
    CrashService,
    CarService,
    AuthService,
    UserService,
    ParkingService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}

import { Component, OnInit } from "@angular/core";
import { CrashService } from "../_services/crash.service";

@Component({
  selector: "app-crashes",
  templateUrl: "./crashes.component.html",
  styleUrls: ["./crashes.component.css"]
})
export class CrashesComponent implements OnInit {
  constructor(crashService: CrashService) {}

  ngOnInit() {}
}

import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { Crash } from "../_models/crash";

@Injectable({
  providedIn: "root"
})
export class CrashService {
  private baseApiUrl = environment.baseApiUrl;
  private crashesUrl = this.baseApiUrl + "/admin/crashes/";

  constructor(private httpClient: HttpClient) {}

  getAll() {
    return this.httpClient.get<Crash[]>(this.crashesUrl);
  }

  update(crash: Crash) {
    return this.httpClient.put(this.crashesUrl + crash.id, crash);
  }

  delete(id: number) {
    return this.httpClient.delete(this.crashesUrl + id);
  }
}

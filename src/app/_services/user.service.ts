import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { User } from "../_models/user";

@Injectable({ providedIn: "root" })
export class UserService {
  baseApiUrl = environment.baseApiUrl;
  constructor(private http: HttpClient) {}
  register(user: User) {
    return this.http.post(this.baseApiUrl + "/auth/register", user);
  }
}

import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { tap } from "rxjs/operators";
import { environment } from "../../environments/environment";

export interface Credentials {
  username: string;
  token: string;
  exp: string;
  verified: boolean;
}

export interface LoginContext {
  username: string;
  password: string;
}

const credentialsKey = "credentials";

@Injectable({
  providedIn: "root"
})
export class AuthService {
  private _credentials: Credentials | null;
  baseApiUrl = environment.baseApiUrl;

  constructor(private httpClient: HttpClient) {
    const savedCredentials =
      sessionStorage.getItem(credentialsKey) ||
      localStorage.getItem(credentialsKey);
    if (savedCredentials) {
      this._credentials = JSON.parse(savedCredentials);
    }
  }

  login(context: LoginContext) {
    return this.httpClient.post(this.baseApiUrl + "/auth/login", context).pipe(
      tap(response => {
        this.setCredentials({
          username: response["username"],
          token: response["token"],
          exp: response["exp"],
          verified: false
        });
      })
    );
  }

  logout(): Observable<boolean> {
    this.setCredentials();
    return of(true);
  }

  isAuthenticated(): boolean {
    return !!this.credentials;
  }

  get credentials(): Credentials | null {
    return this._credentials;
  }

  private setCredentials(credentials?: Credentials, remember?: boolean) {
    this._credentials = credentials || null;

    if (credentials) {
      const storage = remember ? localStorage : sessionStorage;
      storage.setItem(credentialsKey, JSON.stringify(credentials));
    } else {
      sessionStorage.removeItem(credentialsKey);
      localStorage.removeItem(credentialsKey);
    }
  }
}

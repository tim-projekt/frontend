import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { Parking } from "../_models/parking";

@Injectable({
  providedIn: "root"
})
export class ParkingService {
  private baseApiUrl = environment.baseApiUrl;
  private parkingUrl = this.baseApiUrl + "/parkings";
  private parkingAdminUrl = this.baseApiUrl + "/admin/parkings/";

  constructor(private httpClient: HttpClient) {}

  getParkings(): Observable<Parking[]> {
    return this.httpClient.get<Parking[]>(this.parkingUrl);
  }
  addParking(parking: Parking) {
    return this.httpClient.post(this.parkingUrl, parking);
  }
  deleteParking(id) {
    return this.httpClient.delete(this.parkingAdminUrl + id);
  }
}

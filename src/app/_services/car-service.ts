import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { Car } from "../_models/car";

@Injectable({
  providedIn: "root"
})
export class CarService {
  private baseApiUrl = environment.baseApiUrl;
  private carsUrl = this.baseApiUrl + "/cars/";
  private adminCarsUrl = this.baseApiUrl + "/admin/cars/";

  constructor(private httpClient: HttpClient) {}

  getCars(): Observable<Car[]> {
    return this.httpClient.get<Car[]>(this.carsUrl);
  }
  addCar(car: Car) {
    return this.httpClient.post(this.adminCarsUrl, car);
  }
  deleteCar(id) {
    return this.httpClient.delete(this.adminCarsUrl + id);
  }
}
